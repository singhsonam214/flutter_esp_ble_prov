import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'flutter_esp_ble_prov_platform_interface.dart';
/// An implementation of [FlutterEspBleProvPlatform] that uses method channels.
class MethodChannelFlutterEspBleProv extends FlutterEspBleProvPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('flutter_esp_ble_prov');

  @override
  Future<String?> getPlatformVersion() async {
    final version =
        await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }

  @override
  Future<String> scanBleDevices(String prefix) async {
    final args = {'prefix': prefix};
    final raw =
        await methodChannel.invokeMethod('scanBleDevices', args);
    
    // Cast the original map to a map with nullable types
   //Map<String,String>? castedMap = Map.from(raw!);
   
  // Check if the casted map is not null before using it
  
    // final List<dynamic> devices = [];
    // if (raw != null) {
    //   devices.addAll(raw);
    // }
    return raw;
  }

  @override
  Future<List<String>> scanWifiNetworks(
      String deviceName, String proofOfPossession) async {
    final args = {
      'deviceName': deviceName,
      'proofOfPossession': proofOfPossession,
    };
    final raw = await methodChannel.invokeMethod<List<Object?>>(
        'scanWifiNetworks', args);
    final List<String> networks = [];
    if (raw != null) {
      networks.addAll(raw.cast<String>());
    }
    return networks;
  }

  @override
  Future<bool?> provisionWifi(String deviceName, String proofOfPossession,
      String ssid, String passphrase) async {
    final args = {
      'deviceName': deviceName,
      'proofOfPossession': proofOfPossession,
      'ssid': ssid,
      'passphrase': passphrase,
    };
    return await methodChannel.invokeMethod<bool?>('provisionWifi', args);
  }
  @override
  Future<bool?> disconnectBle(String deviceName, String proofOfPossession,
      String serviceUUid,String bluetooth)async{
        final args = {
      'deviceName': deviceName,
      'proofOfPossession': proofOfPossession,
      'serviceUUid': serviceUUid,
      'bluetooth': bluetooth
      
    };
    return await methodChannel.invokeMethod<bool?>('disconnectBle',args);
  }
  @override
  Future<bool?> connectToEspDevice(String deviceName, String proofOfPossession,
      String serviceUUid,String bluetooth)async{
        final args = {
      'deviceName': deviceName,
      'proofOfPossession': proofOfPossession,
      'serviceUUid': serviceUUid,
      'bluetooth': bluetooth
      
    };
    return await methodChannel.invokeMethod<bool?>('connectToEspDevice', args);

  }

  @override
  Future<List<int>?> sendCustomData(String path, String data,String deviceName, String proofOfPossession
      ) async {
    final args = {
      'path': path,
      'data': data,
      'deviceName':deviceName,
      'proofOfPossession':proofOfPossession,
      
    };
    return await methodChannel.invokeMethod<List<int>?>('sendCustomData', args);
  }
}
